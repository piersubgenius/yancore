**Yet Another Network COnfig REtriever**<br/>
*Simple utility for backing up network devices configuration.*<br/>
Created by pier_subgenius<br/>
[https://gitlab.com/piersubgenius](https://gitlab.com/piersubgenius)<br/>

Dependencies:<br/> curl, sshpass, openssh, sqlite3, uuid, expect, diff and<br/> mysql (only for YANCoRe MySQL edition).<br/>
<br/>
Supported device types: cisco, ciscoenable, mikrotik, patton, aethra, huaweine<br/>
<br/>
It can be used with cron, for example:<br/>
*00 23 * * * /usr/bin/screen -dmS yancore /usr/local/bin/yancore backup<br/>*
yancore will be executed every day at 23:00 for all hosts.<br/>
*00 23 * * * /usr/bin/screen -dmS yancore /usr/local/bin/yancore backup group core<br/>*
yancore will be executed every day at 23:00 for group core.<br/>
*00 23 * * * /usr/bin/screen -dmS yancore /usr/local/bin/yancore devtype cisco<br/>*
yancore will be executed every day at 23:00 for devtype cisco.<br/>
<br/>

**YANCoRe MySQL Edition:**
```
Set variables inside yancore.mysql:

#Database host
YMYSQLHOST="127.0.0.1"

#Database username
YMYSQLUSER="user"

#Database password
YMYSQLPASS="password"

#Database name
YMYSQLDATB="yancore"

Import yancore.mysql.sql dump to mysql database:
mysql -u username -p password -h host yancore < yancore.mysql.sql
```
<br/>
<br/>
Type yancore without commands for help.<br/>

**Commands:**
```
backup				Backup commands
config				Configuration commands
export				Export commands
import				Import commands
diff				Compare UUID line by line
log				Show log commands
lsdata				List backed up data commands
hostcount			Count configured hosts commands
purge				Purge commands
ttest				Send Telegram test message
license				Show YANCoRe license

```
**Backup commands:**
```
backup all                  Start backup for all host
backup host [HOST]          Start backup for a specific host
backup group [GROUP]        Start backup for a specific group
backup devtype [DEVTYPE]    Start backup for all specific device type
```
**Config commands:**
```
config show				Show current configuration
config show sensitive			Show current configuration with sensitive information
config [ITEM] [PARAMETER]		Configuration sintax
config telegramtoken			Configure Telegram token
config host				Add or update host
config no host [HOST]			Remove a host
config no telegramtoken [TTOKEN]	Remove a Telegram token
```
config show example:
```
current configuration:

!
!! yancore version 1.x.x
!! yancoredb /var/lib/yancore/yancore.db
!! yancoredbpath /var/lib/yancore
!! yancore database version 1.x.x
!
yancore config telegramtoken 123456790:AAAABBBBCCCCDDDDEEEEFFFFGGGG telegramchatid 1234567890
!
yancore config host 192.168.1.1 hostname router username admin password admin sshport 22 devtype ciscoenable group core enablepass password 
!
yancore config end
```
**Export commands:**
```
export config [CONFIGFILE]      Export configuration to file
export log [CSVFILE]            Export all logs to file in CSV format
export data [UUID] terminal     Export data for a specific backup uuid to terminal
export data [UUID] [FILE]       Export data for a specific backup uuid
```
**Import commands:**
```
import config [CONFIGFILE]      Import configuration from file
```
**Purge commands:**
```
log                             Purge all logs
data                            Purge backed up data
```
Purge data:
```
Sintax: purge data before day [DAY] month [MONTH] year [YEAR]
```
**Tips:**
```
For Cisco NXOS use "cisco" device type
For Cisco ASR1K and ASR9K user "ciscoenable" device type
For Cisco Catalyst user "ciscoenable" device type
```
