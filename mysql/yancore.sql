-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `config` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `DBVER` text NOT NULL,
  `TGRAMTOKEN` text,
  `TGRAMCHATID` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'1.1.6','1234567890:AABBCCDDEEFFGGHHII','1234567890');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `day` text NOT NULL,
  `month` text NOT NULL,
  `year` text NOT NULL,
  `time` text NOT NULL,
  `host` text NOT NULL,
  `hostname` text NOT NULL,
  `uuid` text NOT NULL,
  `data` longblob NOT NULL,
  `datatype` text NOT NULL,
  `devtype` text NOT NULL,
  `grp` text NOT NULL,
  PRIMARY KEY (`ID`),
  FULLTEXT KEY `data_hostname_IDX` (`hostname`),
  FULLTEXT KEY `data_host_IDX` (`host`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hosts`
--

DROP TABLE IF EXISTS `hosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hosts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `host` text NOT NULL,
  `hostname` text NOT NULL,
  `hostdesc` text,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `enablepass` text,
  `sshport` int NOT NULL,
  `devtype` text NOT NULL,
  `grp` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`,`host`(100),`hostname`(100))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `day` text NOT NULL,
  `month` text NOT NULL,
  `year` text NOT NULL,
  `time` text NOT NULL,
  `event` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

